import urllib.parse
import re
from bs4 import BeautifulSoup
import boto3
from werkzeug.exceptions import Unauthorized
import requests
import json
import flask
from flask import Flask, request, Response, make_response, jsonify
from flask import send_from_directory
from flask import send_file
from flask_cors import CORS
from flask_restplus import Api, Resource, inputs, fields, marshal
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property


# config
app = flask.Flask(__name__)
app.config["PROPAGATE_EXCEPTIONS"] = False
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
api = Api(app, version='1.0', title='manes_Api')

CORS(app, origins=r'*', allow_headers=r'*')


# parser for manes-login
parser = api.parser()
parser.add_argument('token', type=str, required=False,
                    help='Bearer Access Token.')

parser.add_argument(
    "username", type=str, required=True, help="username")

parser.add_argument(
    "password", type=str, required=True, help="password")

# parser for get-megssage-manes
parser_manes_get_message = api.parser()
parser_manes_get_message.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_manes_get_message.add_argument(
    "cookies", type=str, required=True, help="object of cookie values")

# parser for manes-send-message
parser_manes_send_message = api.parser()

parser_manes_send_message.add_argument('token', type=str, required=False,
                                      help='Bearer Access Token.')

parser_manes_send_message.add_argument(
    "JSON", type=str, required=False, help="JSON string")

# parser for add_notes
parser_manes_add_notes = api.parser()

parser_manes_add_notes.add_argument('token', type=str, required=False,
                                      help='Bearer Access Token.')

parser_manes_add_notes.add_argument(
    "json", type=str, required=False, help="json")

# parser for add important note
parser_manes_add_imp_note = api.parser()
parser_manes_add_imp_note.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_manes_add_imp_note.add_argument(
    "cookie", type=str, required=True, help="object of cookies")

parser_manes_add_imp_note.add_argument(
    "note", type=str, required=True, help="Note to be added")

# parser for accept friend request
parser_manes_accept_friends = api.parser()
parser_manes_accept_friends.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_manes_accept_friends.add_argument(
    "accept_id", type=str, required=True, help="ID for Friendrequest")

# parser for statistics
parser_manes_get_stats = api.parser()
parser_manes_get_stats.add_argument('token', type=str, required=False,
                                     help='Bearer Access Token.')

parser_manes_get_stats.add_argument(
    "mod_id", type=str, required=False, help="morderator_profile.id")

# define for auth
parser_credentials = api.parser()

parser_credentials.add_argument(
    "username", type=str, required=True, help="username")

parser_credentials.add_argument(
    "password", type=str, required=True, help="password")

# models
token = fields.String(example='"eyJraWQiOiI5M2NHeEY5dHhINTl2SWJpVDR1NMSQQ0VvOFNGZ2xEdno5THg1UVRcL2k3Yz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJjOWRmNTUzMC0zOGQ3LTQzMTEtODM5Zi00OThmNTMzZDU0ZmYiLCJjb2duaXRvOmdyb3VwcyI6WyJiYXNpY3BsYW4iXSwiZXZlbnRfaWQiOiI5MzA2Yjk3NC1hZWE3LTQ4MGYtYjY4Yi1kZDhjZTkzMzhmZmMiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiYXV0aF90aW1lIjoxNjAyMDY1NDEzLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9uZTYwczlEUXkiLCJleHAiOjE2MDIwNjkwMTMsImlhdCI6MTYwMjA2NTQxMywianRpIjoiN2QxYjI4YmYtYmIxNS00MzcyLWFiYjktN2ExZmRmMDUzOWM2IiwiY2xpZW50X2lkIjoiM3JzanRidHNmMmJ2NWYxODdkOTk4OWQyc2wiLCJ1c2VybmFtZSI6ImM5ZGY1NTMwLTM4ZDctNDMxMS04MzlmLTQ5OGY1MzNkNTRmZiJ9.RS-tvI0-Fdb8gvJAxvr2jVSs3ChhPAlXEGGkJu2EoA28PgVdKFfqui9IRzHtGVZi4CDpRs8ZAsA7g7FApF5bJmONnWeu2WWpghMsuEZFixiaEfVlCajhukMZZZC_KiAyxCdKGwti5_DbvWpmGBwdqdlQOWOLhZFPCm2XgnxFp_3ANrqZEJhXwGDTekxl-AMyb1L1s-msE6-5EgVqv9ZggTg9eEwNFc_VfKw87ahDP8sK9Qgz6ZPTsozhCo9vXMQ-YfAlAAicIiK-nJoSwo3fhjLMF-rgcFPySDS9ePWKb08vJfyWLp3-itZHwmn6d5ULKJkE0Q8e-5SSUNfUIX1dxQ"')


# login für manes
@api.route('/manes-login', doc={"description": "Login at manes with manes credentials"})
class Loginmanes(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser)
    def post(self):
        args = parser.parse_args()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # Log into the chat
        isLogged = login_manes(args["username"], args["password"])

        return isLogged

# login manes parameter l for login-id and p for password
def login_manes(l_id, psswd):
    url = "https://www.myloves.com/requests/user.php"

    payload={
        'login_email': l_id,
        'login_pass': psswd,
        'action': 'login'}

    response = requests.request("POST", url, data=payload)

    if 'Error' not in response.text:
        cj = requests.utils.dict_from_cookiejar(response.cookies)
        return cj
    else:
        raise ValueError("Error, please check credentials")


@api.route('/manes-get-message', doc={"description": "check for new messages"})
class getMessagemanes(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_manes_get_message)
    def post(self):
        args = request.get_json()
        global chat_obj
        chat_obj = {}

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get chats
        rslt = get_infos_manes(args)
        return rslt

# get message
def get_infos_manes(cookies):

    url = "https://www.myloves.com/requests/mods.php"


    payload='action=panel&mod={}&edit=0&edit_leader=0'.format(cookies['user'])
    headers = {
        'method': 'POST',
        'accept': '*/*',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'cookie': 'user={}; PHPSESSID={};'.format(cookies['user'],cookies['PHPSESSID']),
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    return sort_infos(response.text, cookies)

def sort_infos(html, cookies):
    parsed_html_1 =  BeautifulSoup(html, features="html.parser")

    no_msg = re.split('>|<',str(parsed_html_1.find_all("strong")[0]))[2] == "Keine offenen Chats."
    
    if not no_msg:
        c_row = parsed_html_1.find_all("div", {"data-target": "#userBlock"})[0]
        f_row = parsed_html_1.find_all("div", {"data-target": "#user2Block"})[0]
        c_info = c_row.find_all('div', {"class": "flex text-muted"})
        f_info = f_row.find_all('div', {"class": "flex text-muted"})
        customer = {}
        fake = {}
        
        #customer nickname & age
        c_nick = c_row.find_all("strong", {"data-type": "text"})[0].text
        customer["nick"] = c_nick.split(',')[0]

        #customer infos
        for div in c_info:
            if 'Name:' in div.text:
                c_name = div.text.split(':')[1]
                customer["name"] = c_name
            elif 'Geburtstag:' in div.text:
                c_bd = div.text.split(':')[1]
                customer["age"] = c_nick.split(', ')[1]
                customer["bday"] = c_bd
            elif 'Kommt aus PLZ:' in div.text:
                c_zip = div.text.split(':')[1]
                customer["zip"] = c_zip
            elif 'Beziehungsstatus' in div.text:
                c_rel = div.text.split(':')[1]
                customer["status"] = c_rel
            elif 'Bio:' in div.text:
                c_bio = div.text.split(':')[1]
                customer["bio"] = c_bio
        
        #customer notes
        c_note = parsed_html_1.find_all("textarea", {"id": "clientNote"})[0].text
        customer["notes"] = c_note

        #customer sex
        c_male = c_row.find("li", {"class": "gender-1"})
        c_female = c_row.find("li", {"class": "gender-2"})

        if len(str(c_male)) > len(str(c_female)):
            customer["sex"] = 'male'
        else:
            customer["sex"] = 'female'

        #customer profile pic
        c_img = c_row.find("img", {"data-real-user": "profile_photo"})
        customer['img_url'] = c_img['src']

        #fake nickname & age
        f_nick = f_row.find_all("strong", {"data-type": "text"})[0].text
        fake["nick"] = f_nick.split(',')[0]

        #fake infos
        for div in f_info:
            if 'Name:' in div.text:
                f_name = div.text.split(':')[1]
                fake["name"] = f_name
            elif 'Geburtstag:' in div.text:
                f_bd = div.text.split(':')[1]
                fake["age"] = f_nick.split(', ')[1]
                fake["bday"] = f_bd
            elif '\nKunde sieht: Kommt aus\n' in div.text:
                f_zip = f_zip = div.text.split('Kommt aus')[1].split('\n')[1]
                fake["zip"] = f_zip
            elif 'Beziehungsstatus' in div.text:
                f_rel = div.text.split(':')[1]
                fake["status"] = f_rel
            elif 'Bio:' in div.text:
                f_bio = div.text.split(':')[1]
                fake["bio"] = f_bio
        
        #fake notes
        f_note = parsed_html_1.find_all("textarea", {"id": "fakeNote"})[0].text
        fake["notes"] = f_note

        #fake sex
        f_male = f_row.find("li", {"class": "gender-1"})
        f_female = f_row.find("li", {"class": "gender-2"})

        if len(str(f_male)) > len(str(f_female)):
            fake["sex"] = "male"
        else:
            fake["sex"] = "female"

        #fake profile pic
        f_img = f_row.find("img", {"data-real-user": "profile_photo"})
        fake['img_url'] = f_img['src']

        #Conversation important notes
        #Parse notes as html
        imp_note_ul = parsed_html_1.find_all("ul", {"id": "specialNotes"})
        imp_note_html = BeautifulSoup(str(imp_note_ul), features="html.parser")
        imp_note_sp = imp_note_html.find_all("span")

        #create object for notes
        imp_notes = []
        #iterate throught the notes and sort time & msg
        if len(imp_note_sp) > 0:
            for s in imp_note_sp:
                n_val = re.split('[0-9][0-9]:[0-9][0-9]: |<',str(s))[2]
                n_date = re.findall('[0-9][0-9].[0-9][0-9].[0-9][0-9] [0-9][0-9]:[0-9][0-9]', str(s))[0]
                imp_notes.append({'date': n_date, 'note': n_val})

        #dialog infos
        #msg count
        msg_no = c_row.find_all("span", {"class": "text-muted"})
        msg_ins = str(msg_no[0]).split(': ')[1].split(' ')[0]
        msg_outs = str(msg_no[0]).split(': ')[2].split('<')[0]
        msg_tot = int(msg_ins) + int(msg_outs)
        chat_obj["count"] = msg_tot

        # prep char-count
        char_count = parsed_html_1.find_all("span", {"class": "char-count"})
        chat_obj['min-char'] = char_count[0].text

        #get stats
        stats_before = parsed_html_1.find_all("div", {"class": "ml-auto"})
        in_today = stats_before[0].text.split('\n')[1]
        out_today = stats_before[1].text.split('\n')[1]
        in_monthly = stats_before[6].text.split('>')[0].split('\n')[1]
        out_monthly = stats_before[-1].text.split('>')[0].split('\n')[1]
        stats = {
            "today": {
                "ins": in_today, 
                "outs": out_today
                },
            "monthly": {
                "ins": in_monthly,
                "outs": out_monthly
                }
            }

        #get conversation numbers
        #fake id
        fake_id = html.find('fake_id =')
        fake_id = html[fake_id+10:fake_id+100].split(';')[0]
        fake["fake_id"] = fake_id

        #customer id
        client_id = html.find('client_id =')
        client_id = html[client_id+12:client_id+100].split(';')[0]
        customer["client_id"] = client_id

        #get MODERATOR ID(Name)
        mod_name = parsed_html_1.find_all("button", {"class": "btn"})
        mod_name = str(mod_name[:1][0]).split(')')[0].split('\'')[1]

        #fill up chat_obj with infos
        chat_obj["customer"] = customer
        chat_obj["mod_name"] = mod_name
        chat_obj["fake"] = fake
        chat_obj["imp_notes"] = imp_notes
        chat_obj["stats"] = stats
    else:
        raise ValueError("Sorry, there are currently no messages")

    return get_message_manes(cookies, fake_id, client_id)

def get_message_manes(cookies, customer_id, fake_id):

    url = "https://www.myloves.com/requests/mods.php"

    payload='action=loadChat&fake={}&client={}'.format(fake_id, customer_id)
    headers = {
        'method': 'POST',
        'accept': '*/*',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'cookie': 'user={}; PHPSESSID={};'.format(cookies['user'],cookies['PHPSESSID']),
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    return prepare_message_manes(response.text)

def prepare_message_manes(html):
    #get messages, date, time
    msg_lst = []
    parsed_html_msg = BeautifulSoup(html, features="html.parser")
    divs = parsed_html_msg.find_all('div')
    i = -1
    

    for div in divs:
        i += 1
        if div.get('class', '') == ['message-break']:
            msg_lst.append({'type': 'newDay', 'key': i,  'date': div.text.split('\n')[2]})
        elif "received" in div.get('class'):
            s = re.split('>',str(div))
            if (any("<img alt" in st for st in s)):
                for l in s:
                    if '<img alt="' in l:
                        found = l.find('src="')
                        img_src = l[found + 5:300].split('"')[0]
                        msg = img_src
                        is_img = True
            else:
                msg = s[1].split('<')[0].split('\n')[1]
                is_img = False
                
            msg_lst.append({
                'type': 'out',
                'key': i,
                'img': is_img,                
                'msg': msg,
                'time': s[3].split('<')[0]
            })
        elif "sent" in div.get('class'):
            s = re.split('>',str(div))
            if (any("<img alt" in st for st in s)):
                for l in s:
                    if '<img alt="' in l:
                        found = l.find('src="')
                        img_src = l[found + 5:300].split('"')[0]
                        msg = img_src
                        is_img = True
            else:
                msg = s[1].split('<')[0].split('\n')[1]
                is_img = False
                
            msg_lst.append({
                'type': 'in',
                'key': i,
                'img': is_img,                
                'msg': msg,
                'time': s[-4].split('<')[0]
            })
    
    chat_obj['messages'] = msg_lst

    #count reminder
    reminder = 0
    for i in msg_lst[::-1]:
        if i['type'] == 'out':
            reminder += 1
        else:
            break
    
    chat_obj['asa'] = reminder

    return chat_obj


# senden für manes
@api.route('/manes-send-message', doc={"description": "send message"})
class getMessagemanes(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_manes_send_message)
    def post(self):

        args = parser_manes_send_message.parse_args()
        args_body = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = manes_send_message(args_body)

def manes_send_message(args):
    #set required params
    customer_id = args['client_id']
    fake_id = args['fake_id']
    mod_id = args['mod_id']
    fake_name = args['fake_name']
    fake_link = args['fake_link']
    reminder = args['reminder']
    time_left = args['time_left']
    message = args['message']
    cookies = args['key']
    query_1 = create_query1(fake_id, customer_id, fake_link, fake_name, message)
    query_2 = create_query2(fake_id, customer_id, message, mod_id, reminder, time_left)

    send_message_1(query_1, cookies)
    send_message_2(query_2, reminder, cookies)

    return 'message sent'

def create_query2(fake_id, customer_id, msg, mod_id, reminder, time_left):
    query_2 = "{}[message]{}[message]{}[message]text[message]mod[message]{}[message]{}[message]No[message]No[message]{}".format(fake_id, customer_id, msg, mod_id, reminder, time_left)
    return query_2

def create_query1(fake_id, customer_id, fake_link, fake_name, msg):
    query_1 = "{}[rt]{}[rt]{}[rt]{}[rt]{}[rt]text".format(fake_id, customer_id, fake_link, fake_name, msg)
    return query_1

def send_message_1(query, cookies):
    url = "https://www.myloves.com/requests/rt.php?"

    params = {
        'action': 'message',
        'query': query
    }

    headers = {
        'method': 'POST',
        'accept': '*/*',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'cookie': 'user={}; PHPSESSID={};'.format(cookies['user'],cookies['PHPSESSID']),
    }

    response = requests.request("GET", url, headers=headers, params=params)
    return response

def send_message_2(query, reminder, cookies):
    url = "https://www.myloves.com/requests/api.php?"

    params = {
        'action': 'sendMessageMod',
        'query': query,
        'reminder': reminder
    }

    headers = {
        'method': 'POST',
        'accept': '*/*',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'cookie': 'user={}; PHPSESSID={};'.format(cookies['user'],cookies['PHPSESSID']),
    }

    response = requests.request("GET", url, headers=headers, params=params)
    return response


# add notes manes
@api.route('/manes-add-notes', doc={"description": "send message"})
class getMessagemanes(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_manes_add_notes)
    def post(self):

        args = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = manes_add_notes(args)

# param expected to decide which of the notes must be changed/added
def manes_add_notes(args):
    cookies = args['cookies']
    to_update = None
    if args['sex'] == "client":
        to_update = 'client_user'
    else:
        to_update = 'fake'

    #update user notes
    headers = {
        'authority': 'www.myloves.com',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'x-requested-with': 'XMLHttpRequest',
        'sec-ch-ua-mobile': '?0',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'origin': 'https://www.myloves.com',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://www.myloves.com/mods',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'cookie': 'user={}; PHPSESSID={}'.format(cookies['user'], cookies['PHPSESSID']),
    }

    data = {
    'action': 'updateUserNote',
    'mod': cookies['user'],
    'edit': '0',
    'edit_leader': '0',
    'user': args['client_id'],
    'fake': args['fake_id'],
    'type': to_update,
    'note': args['notes']
    }

    response = requests.post('https://www.myloves.com/requests/mods.php', headers=headers, data=data)
    return response

# Conversation important Notes for manes
@api.route('/manes-add-imp-note', doc={"description": "change important note"})
class getMessagemanes(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_manes_add_imp_note)
    def post(self):

        args = request.get_json()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = manes_add_imp_note(args)
        return rslt
        
def manes_add_imp_note(args):
    # add important note
    cookies = args['cookies']

    headers = {
        'authority': 'www.myloves.com',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'x-requested-with': 'XMLHttpRequest',
        'sec-ch-ua-mobile': '?0',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'origin': 'https://www.myloves.com',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://www.myloves.com/mods',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'cookie': 'user={}; PHPSESSID={};'.format(cookies['user'],cookies['PHPSESSID']),
    }

    data = {
    'action': 'specialNote',
    'mod': cookies['user'],
    'edit': '0',
    'edit_leader': '0',
    'client': args['client_id'],
    'fake': args['fake_id'],
    'mod_name': args['mod_name'],
    'note': args['notes']
    }

    response = requests.post('https://www.myloves.com/requests/mods.php', headers=headers, data=data)
    return response.text



# accept friends manes
@api.route('/manes-accept-friend', doc={"description": "change important note"})
class getMessagemanes(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_manes_add_imp_note)
    def post(self):

        args = request.get_json()
        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = manes_accept_friend(args)
        return rslt
        
def manes_accept_friend(args):

    url = "https://mod-panel.bettsport.net/api/v1/friends/{}/accept".format(args['accept_id'])

    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'bettsportnet_session={};'.format(args['key']['sess_id']),
    }

    response = requests.request("GET", url, headers=headers)
    return response.text


# auth
@api.route("/oauth/token", doc={
    "description": "Returns a Bearer authentication token. The token is valid for 30 minutes."
})
class awsAuth(Resource):
    @api.expect(parser_credentials)
    @api.response(200, 'Success', token)
    @api.response(401, 'Unauthorized Error')
    def get(self):
        args = parser_credentials.parse_args()
        username = args["username"]
        password = args["password"]
        try:
            client = boto3.client("cognito-idp", "eu-central-1")
            response = client.initiate_auth(
                AuthFlow="USER_PASSWORD_AUTH",
                AuthParameters={"USERNAME": username, "PASSWORD": password, },
                ClientId="41ft5d2kpm37ctefvbf0pb3m29",
                ClientMetadata={"UserPoolId": "eu-central-1_o8ZrmNcQP"},
            )
        except:
            raise Unauthorized('That email/password combination is not valid.')

        Token = response["AuthenticationResult"]["AccessToken"]
        return Token


# get user
def get_user_name(token):
    region = "eu-central-1"
    provider_client = boto3.client("cognito-idp", region_name=region)
    try:
        user = provider_client.get_user(AccessToken=token)
    except provider_client.exceptions.NotAuthorizedException as e:
        raise Unauthorized('Invalid token!')
    return user["Username"]


@ api.errorhandler(ValueError)
def handle_value_exception(error):
    err_message = str(error)
    return {'message': err_message}, 400



if __name__ == "__main__":
    app.run()